import pymongo
import json
from elasticsearch import Elasticsearch

f = open(r"C:\Users\meet.diwan\Documents\Training\ELK\MongodbToElasticsearch\psh_python_webserver.json","r")
datafile = json.load(f)
client = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = client["project1"]
mycol = mydb["psh_python"]
def insert_data():
    mycol.insert_many(datafile)

def collection_name():
    print(mydb.list_collection_names())

def find_data(keyword):
    print(type(mycol.find_one({},{'_id':0})))
    #for x in mycol.find({"@timestamp":keyword}):
        #print(type(x))
def connect_elasticsearch():
    _es = None
    _es = Elasticsearch([{'host':'localhost', 'port':9200}])
    if _es.ping():
        print("Connected")
    else:
        print("Sorry")
    return _es

def create_index(es_object, index_name):
    created = False
    data = mycol.find_one({},{'_id':0})
    try:
        if not es_object.indices.exists(index_name):
            es_object.index(index=index_name, body=data)
            print('Created Index')
            created =True
    except Exception as ex:
        print(str(ex))
    finally:
        return created
#insert_data()
#collection_name()
#find_data("2020-10-29T12:16:07.900Z")
es_object = connect_elasticsearch()
flag = create_index(es_object, "project11")
print(flag)